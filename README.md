# 3 Beam refactoring report

This file contains tips and recommendations to refactor 3Beam front-end application which is written in React.

1. [Usage of third party libraries](#usage-of-third-party-libraries)
2. [Overuse of Redux](#overuse-of-redux)
3. [Filters and pagination](#filters-and-pagination)
4. [Static keywords and grouping](#static-keywords-and-grouping)
5. [Folder structure](#folder-structure)
6. [Unit and E2E testing](#unit-and-e2e-testing)

## Usage of third party libraries

The application is using lots of used and unused third party libraries that make bundle size really huge. If we open [the site](https://d2ewvmsdgjvaq2.cloudfront.net/), log out and look at the login page we'll see that:

![dom content loaded](./uploads/dom-content-load.png)

### Analyzing bundle size

As seen from the picture above about 4.4 MB of content is being loaded to the browser if the user visits the web app for the first time (a.k.a nothing is cached). Since I am running the local server, it took my browser 951 ms seconds to [parse and load DOM content](https://developer.mozilla.org/en-US/docs/Web/API/Window/DOMContentLoaded_event). Imagine someone with slow 3G connection... If you look at the above image carefully, you will see that main javascript bundle, main css files and background image are the largest files that we are serving. To see what is what our main JavaScript bundle is made of, we can [analyze bundle size with webpack bundle size anaylzer](https://create-react-app.dev/docs/analyzing-the-bundle-size/):

![bundle analyzer output](./uploads/bundle-analyzer-output.png)

This is inside of main JavaScript chunk (the chunk that takes 1.6 MB in the above image - `2.a5f55883.chunk.js`) that you are seeing. This consists of libraries like `fullcalendar`, `react-dates`, `core-js`, `react-dom`, `react-boostrap-table-next`, `reacstrap` etc. I am leaving [bundle analyzer output file ](./uploads/bundle-analyzer-output.html) so you can explore further. This insicates that code splitting (lazy loading) does not work properly in the application since modules like `fullcelandar`, `react-bootstrap-table-next` are not used in login page but are being downloaded into browser ahead of time.

### Analyzing dependency list

If you loook at the `package.json` file you will see lots of dependencies that may or may not being used inside application:

```json
{
  "dependencies": {
    "@coreui/coreui-plugin-chartjs-custom-tooltips": "^1.3.1",
    "@coreui/icons": "0.3.0",
    "@coreui/react": "^2.5.1",
    "@fullcalendar/core": "^4.2.0",
    "@fullcalendar/daygrid": "^4.2.0",
    "@fullcalendar/interaction": "^4.2.0",
    "@fullcalendar/react": "^4.2.0",
    "@fullcalendar/timegrid": "^4.2.0",
    "add": "^2.0.6",
    "aphrodite": "^2.3.1",
    "awesome-debounce-promise": "^2.1.0",
    "axios": "^0.19.0",
    "bootstrap": "^4.3.1",
    "bootstrap-daterangepicker": "^3.0.5",
    "chart.js": "^2.8.0",
    "classnames": "^2.2.6",
    "codemirror": "^5.47.0",
    "core-js": "^3.1.4",
    "date-fns": "^1.30.1",
    "enzyme": "^3.10.0",
    "enzyme-adapter-react-16": "^1.14.0",
    "final-form": "^4.18.5",
    "flag-icon-css": "^3.3.0",
    "font-awesome": "^4.7.0",
    "formik": "^2.2.6",
    "node-sass": "^4.12.0",
    "prop-types": "^15.7.2",
    "react": "^16.8.6",
    "react-app-polyfill": "^1.0.1",
    "react-big-calendar": "^0.21.0",
    "react-bootstrap-daterangepicker": "^4.1.0",
    "react-bootstrap-table": "4.3.1",
    "react-bootstrap-table-next": "^3.2.0",
    "react-bootstrap-table2-paginator": "^2.1.0",
    "react-chartjs-2": "^2.7.6",
    "react-codemirror2": "^6.0.0",
    "react-countdown-now": "^2.1.1",
    "react-dates": "^20.2.5",
    "react-dom": "^16.8.6",
    "react-dropzone": "^10.1.7",
    "react-final-form": "^6.3.0",
    "react-final-form-listeners": "^1.0.2",
    "react-google-maps": "9.4.5",
    "react-grid-layout": "^0.16.6",
    "react-icons": "^3.10.0",
    "react-ladda": "6.0.0",
    "react-preloader-icon": "^1.0.0",
    "react-quill": "1.3.3",
    "react-redux": "^7.1.0",
    "react-redux-toastr": "^7.5.1",
    "react-router-config": "^5.0.1",
    "react-router-dom": "^5.0.1",
    "react-scripts": "3.0.1",
    "react-select": "^3.0.4",
    "react-stars": "^2.2.5",
    "react-stripe-elements": "^4.0.0",
    "react-test-renderer": "^16.8.6",
    "react-text-mask": "^5.4.3",
    "react-text-mask-hoc": "^0.11.0",
    "react-toastify": "^5.2.1",
    "react-with-styles": "^3.2.3",
    "react-with-styles-interface-aphrodite": "^5.0.1",
    "reactjs-onboarding": "^1.1.1",
    "reactstrap": "^8.0.0",
    "redux": "^4.0.4",
    "redux-promise": "^0.6.0",
    "redux-thunk": "^2.3.0",
    "redux-thunk-routine": "^1.0.1",
    "reselect": "^4.0.0",
    "source-map-explorer": "^2.5.2",
    "spinkit": "1.2.5",
    "styled-components": "^4.3.2",
    "yup": "^0.27.0"
  }
}
```

By looking carefully to the dependency list you can see that there are NPM packages that are used for same purposes like `react-final-form`, `final-form`, `formik` and packages that are not even being used like `react-google-maps`, `chart.js` and `codemirror`. These dependecies must be carefully analyzed and must be removed if its existence in project is not necessary. For instance, `core-js` package is used for polyfills but we're using `webpack` (along with `babel`) at the same time to transpile next generation JavaScript code to be supported in older browsers. We may not need `core-js` polyfills here.

There are also bunch of icon libraries that are being imported despite of not being used (`src/App.scss`):

```scss
// Styles
// CoreUI Icons Set
@import "~@coreui/icons/css/coreui-icons.css";
// Import Flag Icons Set
@import "~flag-icon-css/css/flag-icon.min.css";
// Import Font Awesome Icons Set
@import "~font-awesome/css/font-awesome.min.css";
// Import Simple Line Icons Set
@import "~simple-line-icons/css/simple-line-icons.css";
```

For example,font awesome icons are not being used anywhere in the application. Those unused styles and scripts are most propably the result of using [multipurpose admin template](https://coreui.io/react/) which we can verify its use by looking at `public/css/main.css` file:

```css
@charset "UTF-8";
/*!
 * CoreUI Pro - Dashboard UI Kit
 * @version v2.1.14
 * @link https://coreui.io/pro/
 * Copyright (c) 2018 creativeLabs Łukasz Holeczek
 */
/*!
 * Bootstrap v4.3.1 (https://getbootstrap.com/)
 * Copyright 2011-2019 The Bootstrap Authors
 * Copyright 2011-2019 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
```

This admin template is not carefully cleansed from unused dependencies and includes lots of styles and scripts that 3 Beam application does not need. You might also want to look at `sass/main.sass` file to see some unused styles like `toastr` and `select2`.

> **Note**: `sass/main.sass` file is most probably is intended to be compiled to `public/css/main.css` file, but if you compile is using `gulp` (there is `gulpfile.js` in the root folder) you will see that some styles are being corrupted in the dashbord page. This indicates that some changes are made to `public/css/main.css` file afterwards which is not good practice (!). `public/css/main.css` file is output of `sass/main.sass` file and changes should only be made in sass files.

**Problems:**

1. Too much unused CSS
2. SASS to CSS flow is broken (see if you can get `gulp` working)
3. Code splitting might be broken

**Solution:**

Remove unused CSS and JavaScript dependencies. Avoid global CSS files as much as possible: each component should have its own styles and should be loaded inside browser when the component mounts. Review code splitting logic, make sure the user downloads when he/she _really_ needs. Also, large images (like background images on login page) should be in optimal size for web.

## Overuse of Redux

If you want to share data between sibling components in React, you either will pass props from root component down to all children (which is an anti-pattern) or use global state management library like Redux or MobX. Using React's Context API is another choice.

React Redux is used in this application for state management. [React Redux](https://react-redux.js.org/introduction/quick-start) is the official React binding for Redux. It lets your React components read data from a Redux store, and dispatch actions to the store to update data. But first things first:

> Keep in mind that, while Redux is a great addition, [you might not need it](https://medium.com/@dan_abramov/you-might-not-need-redux-be46360cf367).

Now, let's see how `react-redux` is being used in 3 Beam application.

In the `src/store/index.js` file store is being created and reducers are being imported from `src/reducers` folder. That's weird because I would expect definitions, actions and reducers to be inside `store` folder like described in [this article](https://medium.com/hackernoon/tips-on-react-for-large-scale-projects-3f9ece85983d).

In the application itself, each module contains its own [definitions](https://stackoverflow.com/a/37054518), actions, reducers and selectors:

![redux modules in 3 Beam](./uploads/redux-modules.png)

The problem is being introduced when these reducers are being used inside the page. Each page (in this case, each screen: see `screens` folder) has its own `index.js` and `container.js` files. `index.js` file gets everything (state and actions) from the store and passes it to `container.js` as props (`src/screens/Visits/index.js`):

```javascript
const mapStateToProps = (state) =>
  createStructuredSelector({
    dentists: dentistAPI.selectors.getDentists,
    radiologists: radiologistsApi.selectors.getRadiologists,
    userRole: profileAPI.selectors.getUserRole,
    userInfo: profileAPI.selectors.getUserProfileInfo,
    hasLinkedAccounts: profileAPI.selectors.getHasLinkedAccounts,
    visits: visitsAPI.selectors.getVisits,
    visitsByPatient: visitDetailsAPI.selectors.getPastVisits,
    bookingInfo: dashboardAPI.selectors.getBookingInfo,
  });

const mapDispatchToProps = (dispatch) => ({
  profileActions: bindActionCreators(profileAPI.actions, dispatch),
  dentistActions: bindActionCreators(dentistAPI.actions, dispatch),
  visitsActions: bindActionCreators(visitsAPI.actions, dispatch),
  radiologyReportsActions: bindActionCreators(
    radiologyReportsApi.actions,
    dispatch
  ),
  visitDetailsActions: bindActionCreators(visitDetailsAPI.actions, dispatch),
  dashboardActions: bindActionCreators(dashboardAPI.actions, dispatch),
  radiologistsActions: bindActionCreators(radiologistsApi.actions, dispatch),
  logoutAction: () => {
    dispatch(logoutAction());
  },
  patientActions: bindActionCreators(patientsApi.actions, dispatch),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(VisitsContainer)
);
```

Then these actions and states are being passed all the way to leaf components by only props. Meaning that the only point of using Redux here is being ignored and components are sharing data with only props. It becomes annoying to look at where this prop come from when you are working on the leaf component.

**Problems:**

1. There is no need to use Redux for everything you do in the application. The local state is just fine.
2. If you're using Redux, and any component needs data from redux it can just `connect` to it. The "container" logic that is being used here is nonsense.

**Solution:**

Use Redux for only storing user and profile data. If possible, migrate to React Context API (with `useReducer` logic its almost the same with Redux)

## Filters and pagination

Since everyhting stored in Redux store, all records are fetched from API and stored into redux store. Most pages does not have even client side pagination and those that have, do it on front-end. Meaning that if some day there are 30K visits in the database, all of the will be fetched from API and will be paginated in front-end.

The same thing applies for the filters. Filters are made in redux's selectors on client side. (see `src/modules/visits/selectors.js` file for example).

**Problems:**

Pagination and filters are handled on front-end, therefore consuming more resources from user's device.

**Solution:**

Implement server side filtering and pagination.

## Static keywords and grouping

There are plenty of weird logics that have been built into application. One of them is storing some static keywords which prevents newly added records from being shown in front-end without modifying the source code.

### Grouping booking items

For example, booking items are sent to front-end without groupping, and grouping work is happening on client side. If you look at the `src/constants/priceItems.constants.js` (what is the point of suffixing file with `constants` if it is already in `constants` folder? 🤨), you will see some keywords like below:

```javascript
export const selectedXRayJustificationOptionsV2 = [
  "x_ray_it",
  "x_ray_se",
  "x_ray_en",
  "x_ray_tm",
  "x_ray_im",
  "x_ray_dt",
  "x_ray_agea",
  "x_ray_aira",
  "x_ray_sav",
  "x_ray_pfa",
  "x_ray_mi",
  "x_ray_pg",
  "x_ray_rbc",
  "x_ray_dp",
  "x_ray_ds",
  "x_ray_co",
  "x_ray_df",
  "x_ray_pci",
  "x_ray_ft",
  "x_ray_os",
  "x_ray_tmja",
  "x_ray_pt",
  "x_ray_pth",
  "x_ray_ort",
  "x_ray_mal",
  "x_ray_def",
  "x_ray_dam",
];
```

Those keywords are then used inside `src/services/pricing.service.js` to group booking items by `name`:

```javascript
const optionBuilder = (acc, item, visitDetails) => {
  const foundDto = visitDetails.bookingDetailsDto.bookingItemDtos.find((dto) =>
    dto.itemPriceDto.name.startsWith(item)
  );

  if (foundDto) {
    acc.push({
      label: foundDto.itemPriceDto.humanReadablePrice,
      value: foundDto.itemPriceDto.name,
      price: foundDto.itemPriceDto.price,
    });
  }
  return acc;
};
```

It means that if you add new booking items in database, you will have to add their `name`s in the `priceItems.constants.js` file to make sure that new booking items are visible to user. Note that this is also true for other pricing items like "CBCT Formats".

### Grouping justification for x-ray options

Another case about grouping is Justification for X-ray options to show them to dentist, when he/she creates new booking. Justification for X-ray options are grouped into three categories like below (Dental, Ent and Max Fax):

![justification for x-ray options](./uploads/justification-for-x-ray.png)

These items are inside booking items that came from API and also are being grouped in front-end. Since there is not any category id or keyword for grouping, it is done in a very weird way:

```javascript
// Building id list doing this way cause in future we may add a new item that will belong to old group
const dentalIds = range(70, 78);
// DO NOT DO THIS AT HOME
// THIS IS CRAZYNESS
dentalIds.push(...range(105, 111));
const EntIds = range(78, 88);
const maxFaxIds = range(88, 91);
const functor = (ids) => ({ value }) => includes(ids, value);

const groupedOptions = [
  {
    label: "Dental",
    options: options.filter(functor(dentalIds)),
  },
  {
    label: "Ent",
    options: options.filter(functor(EntIds)),
  },
  {
    label: "Max Fax",
    options: options.filter(functor(maxFaxIds)),
  },
];
```

These options are grouped by ids in the following way: items from id 75 to 78 (also from 105 to 111 😆) should be under "Dental" heading, items from id 78 to 88 should be under "Ent" heading, and those that has ids from 88 to 91 should be under "Max Fax" heading.

It means that, ff you add new options that should be under "Dental" heading, you will be doing something like below:

```javascript
dentalIds.push(...range(105, 111));
```

Crazy, right? 👀

**Problems:**
Back-end lacks proper keywords for grouping and filtering.

**Solution:**
Review database structure to build _proper_ way of grouping and do grouping work on back-end. Front-end should only be showing processed data.

## Folder structure

Folder structure and file naming conventions reminds that of Angular application. Naming files like `[name].service.js`, `[name].constants.js` is convention that Angular projects are preserving. The fact that everything related to each module (like actions, reducers, selectors) are kept in one place (`modules` folder) and pages are stored in different folder (`screens` folder) may confuse developers.

Also naming `pages` folder `screens` does not clearly indicate where the routes/pages of application are stored.

I would suggest using folder structure used in [this article](https://medium.com/hackernoon/tips-on-react-for-large-scale-projects-3f9ece85983d). This is more scalable, maintainable and clean.

## Unit and e2e testing

There is not any kind of automated testing in the application to ensure that new developers do not break existing functionality. Create React App comes with [built-in unit testing support](https://create-react-app.dev/docs/running-tests) with `jest` and `reac-testing-library` (or `enzyme`). Moreover, there are [lots of ways](https://css-tricks.com/using-cypress-to-write-tests-for-a-react-application/) to integrate it with e2e testing software like Cypress.
